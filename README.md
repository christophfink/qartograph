QGIS kartograph Plugin
======================

This is a plugin for [Quantum GIS](http://qgis.org), which couples it to [Kartograph.py](http://kartograph.org) – a tool to create SVG vector maps, which can then be interactively animated using the related kartograph.js.
