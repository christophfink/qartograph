# -*- coding: utf-8 -*-
"""
/***************************************************************************
 qartograph
                                 A QGIS plugin
 Using Kartograph.py to create (interactive) SVG/HTML5 vector maps
                             -------------------
        begin                : 2013-12-13
        copyright            : (C) 2013 by Christoph Fink
        email                : christoph.fink@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load qartograph class from file qartograph
    from qartograph import qartograph
    return qartograph(iface)
