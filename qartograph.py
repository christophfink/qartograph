# -*- coding: utf-8 -*-
"""
/***************************************************************************
 qartograph
                                 A QGIS plugin
 Using Kartograph.py to create (interactive) SVG/HTML5 vector maps
                              -------------------
        begin                : 2013-12-13
        copyright            : (C) 2013 by Christoph Fink
        email                : christoph.fink@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from __future__ import print_function

# Import the PyQt and QGIS libraries
from PyQt4 import QtCore,QtGui
from qgis.core import *
from qgis.gui import *

from lib.gui import resources_rc
from lib.gui.qartograph_dockwidget import QartographDock
from lib.proj2kartograph import proj2kartograph
from lib.helpers import *
from lib.mp import RunInBackground



import sys,os,os.path,string,json,tinycss,pyproj,shutil,time
from tempfile import mkdtemp
from datetime import datetime
try:
	from kartograph import Kartograph
except:
	Kartograph=None


class qartograph:

	def __init__(self,iface):
		self.iface=iface
		self.mapcanvas=iface.mapCanvas()
		self.maplayerregistry=QgsMapLayerRegistry().instance()
		self.plugin_dir = os.path.dirname(__file__)
		self.settings=QtCore.QSettings()
		locale = self.settings.value("locale/userLocale")[0:2]
		localePath = os.path.join(self.plugin_dir, 'i18n', 'qartograph_{}.qm'.format(locale))

		if os.path.exists(localePath):
			self.translator = QTranslator()
			self.translator.load(localePath)

			if qVersion() > '4.3.3':
				QCoreApplication.installTranslator(self.translator)

		self.dock = QartographDock(self.iface)

		self.previouslayer=False
		self.workingdir=False


	def initGui(self):
		self.action = QtGui.QAction(
			QtGui.QIcon(":/plugins/qartograph/icon.png"),
			u"Qartograph", self.iface.mainWindow())
		self.action.triggered.connect(self.toggleDock)

		self.iface.addToolBarIcon(self.action)
		self.iface.addPluginToMenu(u"&Qartograph …", self.action)

		self.iface.addDockWidget(QtCore.Qt.RightDockWidgetArea,self.dock)
		if self.settings.value("qartograph/show_dock",0)==(False,):
			self.dock.hide()

		if Kartograph is None:
			self.dock.tabWidget.hide()
			self.dock.saveButton.hide()
			self.dock.noKartographWarning.show()
		else:
			self.dock.tabWidget.show()
			self.dock.saveButton.show()
			self.dock.noKartographWarning.hide()
			
		self.dock.pleaseCheckProj.hide()
		self.dock.noLayersWarning.hide()
		
		self.dock.visibilityChanged.connect(self.updateGui)
		self.mapcanvas.mapRenderer().destinationSrsChanged.connect(self.updateGui)
		self.iface.projectRead.connect(self.updateGui) 
		self.mapcanvas.layersChanged.connect(self.updateGui) 
		self.iface.currentLayerChanged.connect(self.updateGui)
		
		self.iface.projectRead.connect(lambda: QgsProject.instance().writeProject.connect(self.saveAllInputs))
		
		self.dock.saveButton.clicked.connect(self.saveToWebMap_I)
		
		self.dock.comboBox_symbols_layer.currentIndexChanged.connect(self.loadSymbolsFieldDropDown)
		self.dock.comboBox_symbols_attribute.currentIndexChanged.connect(self.saveSymbolsDropDowns)


	def unload(self):
		self.dock.setVisible(False)
		self.iface.removePluginMenu(u"&Qartograph", self.action)
		self.iface.removeToolBarIcon(self.action)


	def toggleDock(self):
		if self.dock.isVisible():
			self.dock.hide()
			self.settings.setValue("qartograph/show_dock",(False,))
		else:
			self.dock.show()
			self.settings.setValue("qartograph/show_dock",(True,))
		self.checkProjection()

	def updateGui(self):
		if self.mapcanvas.layerCount()==0:
			self.dock.noLayersWarning.show()
			self.dock.tabWidget.hide()
			self.dock.saveButton.hide()
		else:
			self.dock.noLayersWarning.hide()
			self.dock.tabWidget.show()
			self.dock.saveButton.show()
			
			self.savePreviousLayerFromTableWidgets()
			self.loadLayerToTableWidgets()
			self.loadSymbolsLayerDropDown()
			self.loadSymbolsFieldDropDown()
			self.checkProjection()

	def checkProjection(self):
		# u'+proj=xxxx xx=yy yy=xx … '
		#proj4params={}
		#for p in self.mapcanvas.mapRenderer().destinationCrs().toProj4().split(' '):
		#	(k,v)=p.split("=") if "=" in p else (p,"")
		#	proj4params[k]=v

		projectionFine=False
		proj4params=proj4StringToDict(self.mapcanvas.mapRenderer().destinationCrs().toProj4())
		if "+proj" in proj4params:
			if proj4params["+proj"] in proj2kartograph:
				projectionFine=True

		if Kartograph is not None and projectionFine:
			self.dock.tabWidget.show()
			self.dock.saveButton.show()
			self.dock.pleaseCheckProj.hide()
		elif not Kartograph is None:
			self.dock.tabWidget.hide()
			self.dock.saveButton.hide()
			self.dock.pleaseCheckProj.show()
			

	def saveLayerToShapefile(self,layerIndex,outputDir):
		try:
			layer=self.maplayerregistry.mapLayer(layerIndex)
			
			other_fnames=[self.mapcanvas.layer(l).customProperty("qartograph_filename","") if self.mapcanvas.layer(l).id()!=layerIndex else "" for l in range(self.mapcanvas.layerCount())] # other layers' filenames
			i=0
			filename=""
			while filename in other_fnames:
				filename="%s_%04d.shp" %(sanitise(layer.title() if layer.title()!="" else layer.name()),i)
				if i==0:
					filename=layer.customProperty("qartograph_filename",filename)
				i+=1
			
			fields=QgsFields() 
			for f in range(min(layer.dataProvider().fields().count(),254)): # shapefiles limit
				# hier später über customProperty subset auswählen!
				fields.append(layer.dataProvider().fields().field(f))
			
			wkbtype=layer.wkbType()
			
			transform=QgsCoordinateTransform(layer.crs(),QgsCoordinateReferenceSystem(4326))
			extent=QgsCoordinateTransform(self.mapcanvas.mapRenderer().destinationCrs(), layer.crs()).transform(self.mapcanvas.extent())
			
			# create vector writer object
			writer=QgsVectorFileWriter(os.path.join(outputDir,filename),"utf-8",fields,wkbtype,QgsCoordinateReferenceSystem(4326),"ESRI Shapefile")
			if writer.hasError()!=QgsVectorFileWriter.NoError:
				raise Exception("Error creating Shapefile %s"%(filename,))

			# for each feature: test whether within extent, transform to wgs84 (kartograph's native internal srs)
			for feature in layer.getFeatures():
				if feature.geometry().intersects(extent):
					f=QgsFeature(feature)
					f.geometry().transform(transform)
					if not writer.addFeature(f):
						raise Exception("Unable to save feature %s to %s" %(f,filename))

			# close writer object and flush
			del writer
		except Exception,e:
			print("Failed to save Shapefile: %s" %(e,))
		finally:
			if layerIndex in self.layersToBeExported:
				self.layersToBeExported.remove(layerIndex)
			

	def loadLayerToTableWidgets(self):
		#self.savePreviousLayerFromTableWidgets()

		self.dock.tableWidget_datafields.clear()
		self.dock.tableWidget_datafields.setRowCount(0)
		self.dock.tableWidget_datafields.setColumnCount(2)
		
		self.dock.tableWidget_tooltips.clear()
		self.dock.tableWidget_tooltips.setRowCount(0)
		self.dock.tableWidget_tooltips.setColumnCount(2)

		try:
			layer=self.mapcanvas.currentLayer()
		except Exception, e:
			#print(e)
			return
		if layer is None:
			return
		if not layer.type()==QgsMapLayer.VectorLayer:
			return
		if not self.iface.legendInterface().isLayerVisible(layer):
			return

		self.previouslayer=layer.id()

		#if json.loads(layer.customProperty("qartograph/fielddict","{}"))=={}:
		#	layer.removeCustomProperty("qartograph/fielddict")
		fields={}
		for f in layer.dataProvider().fields():
			fname=f.comment() if f.comment()!="" else f.name()
			fields[fname]={
				"checked": False,
				"customName": fname,
				"useAsTooltip": False,
				"tooltipLabel": fname,
			}

		fields=json.loads(layer.customProperty("qartograph/fielddict",json.dumps(fields)))

		# datafields
		for f in sorted(fields):
			item1=QtGui.QTableWidgetItem(f)
			item2=QtGui.QTableWidgetItem(fields[f]["customName"])
			item2.setCheckState(QtCore.Qt.Checked if fields[f]["checked"] else QtCore.Qt.Unchecked)
			rowcount=self.dock.tableWidget_datafields.rowCount()
			self.dock.tableWidget_datafields.insertRow(rowcount)
			self.dock.tableWidget_datafields.setItem(rowcount,1,item1) # hidden column -> original name
			self.dock.tableWidget_datafields.setItem(rowcount,0,item2)
		self.dock.tableWidget_datafields.hideColumn(1) # don't show actual name – so can't be altered
		
		# tooltips
		for f in sorted(fields):
			item1=QtGui.QTableWidgetItem(f)
			item2=QtGui.QTableWidgetItem(fields[f]["tooltipLabel"])
			item2.setCheckState(QtCore.Qt.Checked if fields[f]["useAsTooltip"] else QtCore.Qt.Unchecked)
			#item2.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
			rowcount=self.dock.tableWidget_tooltips.rowCount()
			self.dock.tableWidget_tooltips.insertRow(rowcount)
			self.dock.tableWidget_tooltips.setItem(rowcount,0,item2)
			self.dock.tableWidget_tooltips.setItem(rowcount,1,item1)
		self.dock.tableWidget_tooltips.hideColumn(1) 
		

	def savePreviousLayerFromTableWidgets(self):
		if not self.previouslayer:
			return
		try:
			layer=self.maplayerregistry.mapLayer(self.previouslayer)
		except Exception, e:
			print(e)
			return
		if layer is None:
			return
		if not layer.type()==QgsMapLayer.VectorLayer:
			return
				
		fields={}
		for row in range(self.dock.tableWidget_datafields.rowCount()):
			fields[self.dock.tableWidget_datafields.item(row,1).text()]={
				"checked": self.dock.tableWidget_datafields.item(row,0).checkState()==QtCore.Qt.Checked,
				"customName": self.dock.tableWidget_datafields.item(row,0).text(),
				"useAsTooltip": self.dock.tableWidget_tooltips.item(row,0).checkState()==QtCore.Qt.Checked,
				"tooltipLabel": self.dock.tableWidget_tooltips.item(row,0).text()
			}
		for row in range(self.dock.tableWidget_tooltips.rowCount()):
			fields[self.dock.tableWidget_tooltips.item(row,1).text()]["useAsTooltip"]=self.dock.tableWidget_tooltips.item(row,0).checkState()==QtCore.Qt.Checked

		if fields!={}: # can happen if layer visibility is toggled 
			layer.setCustomProperty("qartograph/fielddict",json.dumps(fields))
	
		
	def saveSymbolsDropDowns(self):
		self.saveSymbolsFieldDropDown()
		self.saveSymbolsLayerDropDown()
	
		
	def saveSymbolsLayerDropDown(self):
		project=QgsProject.instance()

		if self.dock.comboBox_symbols_layer.count()>0:
			project.writeEntry("qartograph","layerToShowSymbolsFrom",self.dock.comboBox_symbols_layer.itemData(self.dock.comboBox_symbols_layer.currentIndex()))
			
		print("wrote %s" %(self.dock.comboBox_symbols_layer.itemData(self.dock.comboBox_symbols_layer.currentIndex()),))
	
			
	def loadSymbolsLayerDropDown(self):
		project=QgsProject.instance()
		
		storedIndex=project.readEntry("qartograph","layerToShowSymbolsFrom","[none]")[0]
		currentIndex=0
		
		self.dock.comboBox_symbols_layer.clear()
		self.dock.comboBox_symbols_layer.addItem("","[none]")
		
		for layer in self.maplayerregistry.mapLayers():
			try:
				layer=self.maplayerregistry.mapLayer(layer)
			except Exception, e:
				print(e)
				continue
			if layer is None:
				continue
			if not layer.type()==QgsMapLayer.VectorLayer:
				continue
			self.dock.comboBox_symbols_layer.addItem(
				layer.title() if layer.title()!="" else layer.name(),
				layer.id()
			)
			if layer.id()==storedIndex:
				currentIndex=self.dock.comboBox_symbols_layer.count()-1

		self.dock.comboBox_symbols_layer.setCurrentIndex(currentIndex)


	def saveSymbolsFieldDropDown(self):
		try:
			layer=self.maplayerregistry.mapLayer(self.dock.comboBox_symbols_layer.itemData(self.dock.comboBox_symbols_layer.currentIndex()))
		except Exception, e:
			print(e)
			return
		if layer is None:
			return
		if not layer.type()==QgsMapLayer.VectorLayer:
			return

		if self.dock.comboBox_symbols_attribute.count()>0:
			layer.setCustomProperty("qartograph/fieldToShowSymbolsFrom",self.dock.comboBox_symbols_attribute.itemData(self.dock.comboBox_symbols_attribute.currentIndex()))
			
	
	def loadSymbolsFieldDropDown(self):
		try:
			layer=self.maplayerregistry.mapLayer(self.dock.comboBox_symbols_layer.itemData(self.dock.comboBox_symbols_layer.currentIndex()))
		except Exception, e:
			print(e)
			self.dock.comboBox_symbols_attribute.clear()
			return
		if layer is None:
			self.dock.comboBox_symbols_attribute.clear()
			return
		if not layer.type()==QgsMapLayer.VectorLayer:
			self.dock.comboBox_symbols_attribute.clear()
			return

		selectedField=layer.customProperty("qartograph/fieldToShowSymbolsFrom","[none]")
		currentIndex=0
	
		self.dock.comboBox_symbols_attribute.clear()
		self.dock.comboBox_symbols_attribute.addItem("","[none]")		

		for f in layer.dataProvider().fields():
			self.dock.comboBox_symbols_attribute.addItem(
				f.comment() if f.comment()!="" else f.name(),
				f.name()
			)
			if f.name()==selectedField:
				currentIndex=self.dock.comboBox_symbols_attribute.count()-1

		self.dock.comboBox_symbols_attribute.setCurrentIndex(currentIndex)
					
	
	def getCurrentLayerSymbology(self):
		self.savePreviousLayerFromTableWidgets()
		self.previouslayer=self.mapcanvas.currentLayer().id()
		self.savePreviousLayerFromTableWidgets()
		
		try:
			layer=self.mapcanvas.currentLayer()
		except Exception, e:
			print(e)
			return
		if layer is None:
			return None
		if not layer.type()==QgsMapLayer.VectorLayer:
			return None
			
		renderer=layer.rendererV2()
		if renderer.type()=='singleSymbol':
			pass
		elif renderer.type()=='categorizedSymbol':
			pass
		elif renderer.type()=='graduatedSymbol':
			pass
		elif renderer.type()=='RuleRenderer':
			pass
		elif renderer.type()=='pointDisplacement':
			pass
				
	
	def saveKartographControlFiles(self):
		pass			
	
	
	def runKartograph(self):
		pass


	def insertIntoTemplate(self,template="default"):
		pass


	def saveAllInputs(self):
		self.savePreviousLayerFromTableWidgets()
		self.previouslayer=self.mapcanvas.currentLayer().id()
		self.savePreviousLayerFromTableWidgets()


	def saveToWebMap_I(self): # called when button clicked
		if not self.workingdir:
			self.workingdir=mkdtemp("-qartograph")
		if not os.path.exists(os.path.join(workingdir,"geodata")) and os.path.isdir(os.path.join(workingdir,"geodata")):
			try:
				shutil.rmtree(os.path.join(workingdir,"geodata"))
			except:
				pass
			os.mkdir(os.path.join(workingdir,"geodata"))
		print(self.workingdir)
		
		self.layersToBeExported=[]
		for l in range(self.mapcanvas.layerCount()):
			self.layersToBeExported.append(self.mapcanvas.layer(l).id())
			
		for l in range(self.mapcanvas.layerCount()):
			layerIndex=self.mapcanvas.layer(l).id()
			RunInBackground(self.saveLayerToShapefile,(layerIndex,self.workingdir),self.saveToWebMap_II)

		self.saveKartographControlFiles()
		
		self.destination=QtGui.QFileDialog.getExistingDirectory(None,"Please specify an output path")
		if self.destination is None or self.destination=="":
			return
	
			
	def saveToWebMap_II(self): # callback
		if len(self.layersToBeExported)<=0:
			RunInBackground(self.runKartograph(), (),self.saveToWebMap_III)
	
			
	def saveToWebMap_III(self):
		# ...
		
		openWithDefaultApplication(self.destination)
