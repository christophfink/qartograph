#!/bin/env python
# -*- coding: utf-8 -*-
# altered from: http://stackoverflow.com/questions/2581817/python-subprocess-callback-when-cmd-exits

from __future__ import print_function
import multiprocessing

def RunInBackground(func,arguments=(),callback=None):
	def Background(func,arguments,callback):
		func(*arguments)
		if callback is not None:
			callback()
		return

	p=multiprocessing.Process(target=Background,args=(func,arguments,callback))
	p.start()
	return p


# test routine
if __name__ == '__main__':
	from time import sleep
	import random
	def mul(a, b):
		for i in range(a):
			sleep(0.5*random.random())
		return a * b
	def cb():
		print("finished")
	def main():
		print("hä")
		RunInBackground(mul,(4,5),cb)
		print("started")
		print("exitmain")
	main()


