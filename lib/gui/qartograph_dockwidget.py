# -*- coding: utf-8 -*-
from __future__ import print_function

from PyQt4 import QtGui,QtCore
from ui_qartograph import Ui_Qartograph

class QartographDock(QtGui.QDockWidget, Ui_Qartograph):
	def __init__(self, iface):
		self.iface = iface
		self.mapCanvas = iface.mapCanvas()
		self.renderer = self.mapCanvas.mapRenderer()
		
		QtGui.QDockWidget.__init__(self)
		self.setupUi(self)
		self.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)
	
		# settings = QSettings()
	        #if not settings.contains(self.USE_CATEGORIES):
	        #    settings.setValue(self.USE_CATEGORIES, False)
	        #useCategories = settings.value(self.USE_CATEGORIES, type=bool)
	        #if useCategories:
	        #    self.modeComboBox.setCurrentIndex(0)
	        #else:
	        #    self.modeComboBox.setCurrentIndex(1)
	        #self.modeComboBox.currentIndexChanged.connect(self.modeHasChanged)


