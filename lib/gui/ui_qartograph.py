# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_qartograph.ui'
#
# Created: Mon Apr  7 16:10:14 2014
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Qartograph(object):
    def setupUi(self, Qartograph):
        Qartograph.setObjectName(_fromUtf8("Qartograph"))
        Qartograph.resize(387, 887)
        self.dockWidgetContents = QtGui.QWidget()
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.verticalLayout = QtGui.QVBoxLayout(self.dockWidgetContents)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.noLayersWarning = QtGui.QLabel(self.dockWidgetContents)
        self.noLayersWarning.setAlignment(QtCore.Qt.AlignCenter)
        self.noLayersWarning.setMargin(10)
        self.noLayersWarning.setObjectName(_fromUtf8("noLayersWarning"))
        self.verticalLayout.addWidget(self.noLayersWarning)
        self.noKartographWarning = QtGui.QLabel(self.dockWidgetContents)
        self.noKartographWarning.setWordWrap(True)
        self.noKartographWarning.setMargin(10)
        self.noKartographWarning.setObjectName(_fromUtf8("noKartographWarning"))
        self.verticalLayout.addWidget(self.noKartographWarning)
        self.pleaseCheckProj = QtGui.QLabel(self.dockWidgetContents)
        self.pleaseCheckProj.setWordWrap(True)
        self.pleaseCheckProj.setMargin(10)
        self.pleaseCheckProj.setObjectName(_fromUtf8("pleaseCheckProj"))
        self.verticalLayout.addWidget(self.pleaseCheckProj)
        self.tabWidget = QtGui.QTabWidget(self.dockWidgetContents)
        self.tabWidget.setEnabled(True)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab_datafields = QtGui.QWidget()
        self.tab_datafields.setObjectName(_fromUtf8("tab_datafields"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.tab_datafields)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.label_5 = QtGui.QLabel(self.tab_datafields)
        self.label_5.setWordWrap(True)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayout_4.addWidget(self.label_5)
        self.tableWidget_datafields = QtGui.QTableWidget(self.tab_datafields)
        self.tableWidget_datafields.setShowGrid(False)
        self.tableWidget_datafields.setGridStyle(QtCore.Qt.NoPen)
        self.tableWidget_datafields.setColumnCount(1)
        self.tableWidget_datafields.setObjectName(_fromUtf8("tableWidget_datafields"))
        self.tableWidget_datafields.setRowCount(0)
        self.tableWidget_datafields.horizontalHeader().setVisible(False)
        self.tableWidget_datafields.horizontalHeader().setStretchLastSection(True)
        self.tableWidget_datafields.verticalHeader().setVisible(False)
        self.verticalLayout_4.addWidget(self.tableWidget_datafields)
        self.tabWidget.addTab(self.tab_datafields, _fromUtf8(""))
        self.tab_tooltips = QtGui.QWidget()
        self.tab_tooltips.setObjectName(_fromUtf8("tab_tooltips"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.tab_tooltips)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_4 = QtGui.QLabel(self.tab_tooltips)
        self.label_4.setWordWrap(True)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout_3.addWidget(self.label_4)
        self.tableWidget_tooltips = QtGui.QTableWidget(self.tab_tooltips)
        self.tableWidget_tooltips.setShowGrid(False)
        self.tableWidget_tooltips.setGridStyle(QtCore.Qt.NoPen)
        self.tableWidget_tooltips.setObjectName(_fromUtf8("tableWidget_tooltips"))
        self.tableWidget_tooltips.setColumnCount(0)
        self.tableWidget_tooltips.setRowCount(0)
        self.tableWidget_tooltips.horizontalHeader().setVisible(False)
        self.tableWidget_tooltips.horizontalHeader().setStretchLastSection(True)
        self.tableWidget_tooltips.verticalHeader().setVisible(False)
        self.verticalLayout_3.addWidget(self.tableWidget_tooltips)
        self.tabWidget.addTab(self.tab_tooltips, _fromUtf8(""))
        self.tab_symbols = QtGui.QWidget()
        self.tab_symbols.setObjectName(_fromUtf8("tab_symbols"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.tab_symbols)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label_7 = QtGui.QLabel(self.tab_symbols)
        self.label_7.setTextFormat(QtCore.Qt.RichText)
        self.label_7.setWordWrap(True)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.verticalLayout_2.addWidget(self.label_7)
        self.label_8 = QtGui.QLabel(self.tab_symbols)
        self.label_8.setWordWrap(True)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.verticalLayout_2.addWidget(self.label_8)
        self.comboBox_symbols_layer = QtGui.QComboBox(self.tab_symbols)
        self.comboBox_symbols_layer.setObjectName(_fromUtf8("comboBox_symbols_layer"))
        self.verticalLayout_2.addWidget(self.comboBox_symbols_layer)
        self.comboBox_symbols_attribute = QtGui.QComboBox(self.tab_symbols)
        self.comboBox_symbols_attribute.setObjectName(_fromUtf8("comboBox_symbols_attribute"))
        self.verticalLayout_2.addWidget(self.comboBox_symbols_attribute)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.tabWidget.addTab(self.tab_symbols, _fromUtf8(""))
        self.verticalLayout.addWidget(self.tabWidget)
        self.saveButton = QtGui.QPushButton(self.dockWidgetContents)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.verticalLayout.addWidget(self.saveButton)
        Qartograph.setWidget(self.dockWidgetContents)

        self.retranslateUi(Qartograph)
        self.tabWidget.setCurrentIndex(2)
        QtCore.QMetaObject.connectSlotsByName(Qartograph)

    def retranslateUi(self, Qartograph):
        Qartograph.setWindowTitle(_translate("Qartograph", "Qartograph", None))
        self.noLayersWarning.setText(_translate("Qartograph", "No layers found.", None))
        self.noKartographWarning.setText(_translate("Qartograph", "<html><head/><body><p>Your system does not have Kartograph.py installed. Refer to <a href=\"http://kartograph.org/docs/kartograph.py/\"><span style=\" text-decoration: underline; color:#0000ff;\">http://kartograph.org/</span></a> for instructions.</p></body></html>", None))
        self.pleaseCheckProj.setText(_translate("Qartograph", "<html><head/><body><p>Umm … sorry, but Kartograph.js does not support all spatial reference systems offered by QGIS. The one you chose is not on the list :/</p><p>Please make sure your choice is amongst the following projections: </p><ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">aitoff</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">cea</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">eck4</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">goode</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">hatano</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">laea</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">lcc</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">longlat</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">loxim</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">merc</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">moll</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">natearth</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">nicol</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">ortho</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">robin</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">sinu</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">stere</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">wag4</li><li style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">wag5</li><li style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">wintri</li></ul></body></html>", None))
        self.label_5.setText(_translate("Qartograph", "Attributes to be exported into the web map (double-click on field name to rename field):", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_datafields), _translate("Qartograph", "Data fields", None))
        self.label_4.setText(_translate("Qartograph", "Fields to display information from in mouseover tooltips (double-click to rename fields):", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_tooltips), _translate("Qartograph", "Tooltips", None))
        self.label_7.setText(_translate("Qartograph", "<html><head/><body><p>Kartograph can display “bubble” symbols over the centroid of features. <br/>(see <a href=\"http://kartograph.org/docs/kartograph.js/symbols.html\"><span style=\" text-decoration: underline; color:#0000ff;\">here</span></a> for examples and details)</p></body></html>", None))
        self.label_8.setText(_translate("Qartograph", "Select which attributes of which layer data is taken from to enable “bubbles”.", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_symbols), _translate("Qartograph", "Symbols", None))
        self.saveButton.setText(_translate("Qartograph", "Save to webmap …", None))

