#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import string, platform, os
#from qgis.core import *

def sanitise(filename):
	validchars = frozenset("%s%s" % (string.ascii_letters, string.digits))
	return str("".join(c for c in filename if c in validchars)[:128] )
	

def colortuple(self,qt4color):
	(r,g,b,a)=qt4color.getRgb()
	return "#%02x%02x%02x" %(r,g,b)


def openWithDefaultApplication(filename):
	system=platform.system()
	if system == "Windows":
		os.startfile(filename)
	elif system == "Darwin":
		os.system("open %s" %(filename,))
	else: # let's assume everything else is posix/freedesktop compliant
		os.system("xdg-open %s" %(filename,))

def proj4StringToDict(projstring):
	proj4params={}
	for p in projstring.split(" "): 
		(k,v)=p.split("=") if "=" in p else (p,"")
		proj4params[k]=v
	return proj4params

def main():
	pass
if __name__ == '__main__':
	main()


