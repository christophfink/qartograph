#!/bin/env python
# -*- coding: utf-8 -*-

# dict to translate from proj4 to kartograph
proj2kartograph={
	"lcc": {
		"K_name": "lcc",
		"parameters": {
			"lon0": "lon0",
			"lat0": "lat0",
			"lat1": "lat1",
			"lat2": "lat2"
		}
	},
	"aitoff": {
		"K_name": "aitoff",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"wintri": {
		"K_name": "winkel3",
		"parameters": {
			"lon0": "lon0",
			"lat0": "lat0",
			"lat1": "lat1",
			"lat2": "lat2"
		}
	},
	"stere": {
		"K_name": "stereo",
		"parameters": {
			"lon0": "lon0",
			"lat0": "lat0"
		}
	},
	"laea": {
		"K_name": "laea",
		"parameters": {
			"lon0": "lon0",
			"lat0": "lat0"
		}
	},
	"ortho": {
		"K_name": "ortho",
		"parameters": {}
	},
	"nicol": {
		"K_name": "nicolosi",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"goode": {
		"K_name": "goodehomolosine",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"hatano": {
		"K_name": "hatano",
		"parameters": {}
	},
	"loxim": {
		"K_name": "loximuthal",
		"parameters": {
			"lon0",
			"lat0"
		}
	},
	"wag4": {
		"K_name": "wagner4",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"wag5": {
		"K_name": "wagner5",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"moll": {
		"K_name": "mollweide",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"sinu": {
		"K_name": "sinuosoidal",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"eck4": {
		"K_name": "eckert4",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"robin": {
		"K_name": "robinson",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"natearth": {
		"K_name": "naturalearth",
		"parameters": {}
	},
	"merc": {
		"K_name": "mercator",
		"parameters": {
			"lon0": "lon0"
		}
	},
	"cea": {
		"K_name": "cea",
		"parameters": {
			"lon0": "lon0" # so here it is: behrmann and gallpeters also are cea in proj, but with fixed lat_ts to 30 and 45 … how to resolve this?
		}
	},
	"longlat": {
		"K_name": "lonlat",
		"parameters": {}
	}
}	

# additionally, kartograph supports a "satellite" projection …
